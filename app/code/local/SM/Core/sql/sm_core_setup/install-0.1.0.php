<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$configUpdate = new Mage_Core_Model_Config();
$configUpdate->saveConfig('design/package/name', 'default');
$configUpdate->saveConfig('design/theme/template', 'smart');
$configUpdate->saveConfig('design/theme/skin', 'smart');
$configUpdate->saveConfig('design/theme/layout', 'smart');

$installer->endSetup();
